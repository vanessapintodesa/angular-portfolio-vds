import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { WorkComponent } from './components/work/work.component';
import { ArqComponent } from './components/arq/arq.component';
import { DevComponent } from './components/dev/dev.component';
import { UxComponent } from './components/ux/ux.component';
import { from } from 'rxjs';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'resume', component: WorkComponent },
  { path: 'architecture', component: ArqComponent },
  { path: 'developer', component: DevComponent },
  { path: 'ux', component: UxComponent },

];

@NgModule({
  imports: [
    BrowserAnimationsModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
